package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("*******************");
		System.out.println("Initialized connection to database.");
		System.out.println("*******************");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
	
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int total = num1 + num2;
		
//		The getWriter() method is used to print out information in the browser as a response.
		PrintWriter out = res.getWriter();
		out.println("<h1>The total of the two numbers are " + total+"</h1>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You have accessed the get method of the calculator servlet.</h1>");
	}
	
	public void destroy() {
		System.out.println("*******************");
		System.out.println("Disconnected from database.");
		System.out.println("*******************");
	}
	
}
