package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class ActivityServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3524594551681272753L;

	public void init() throws ServletException {
		System.out.println("*******************");
		System.out.println("Initialized connection to database.");
		System.out.println("*******************");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
	
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int total = 0;
		String opera = req.getParameter("opera");
		if (opera.equals("add")) {
			total = num1 + num2;
		} else if(opera.equals("subtract")) {
			total = num1 - num2;
		} else if(opera.equals("multiply")) {
			total = num1 * num2;
		} else if(opera.equals("divide")) {
			total = num1 / num2;
		}
		
//		int total = num1 + num2;
		
//		The getWriter() method is used to print out information in the browser as a response.
		PrintWriter out = res.getWriter();
		out.println("<h1>The two numbers you provided are: " + num1 +", "+ num2 +"</h1>");
		out.println("<h4>The operation that you wanted is: " + opera + "</h4>");
		out.println("<h4>The result is: " + total+"</h4>");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<h4>To use the app, input two number and an operation.</h4>");
		out.println("<h4>Hit the submit button after filling in the details.</h4>");
		out.println("<h4>You will get the result shown in your browser!</h4>");
	}
	
	public void destroy() {
		System.out.println("*******************");
		System.out.println("Disconnected from database.");
		System.out.println("*******************");
	}
}
